package main

import (
	"io/ioutil"
	"fmt"
	"regexp"
	"strings"
	"unicode/utf8"
	"unicode"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func lowerFirst(s string) string {
	if s == "" {
		return ""
	}
	r, n := utf8.DecodeRuneInString(s)
	return string(unicode.ToLower(r)) + s[n:]
}

func processArr(inputStr string) string {
	if inputStr == "" {
		return ""
	}
	var cleanedInput = strings.Trim(inputStr, "[]")
	if len(cleanedInput) < 1 {
		return ""
	}
	var elements = strings.Split(cleanedInput, " ")
	for index, element := range elements {
		elements[index] = "\"" + element + "\""
	}
	return strings.Join(elements, ", ")
}

func processLog(inputStr string) string {
	var reGroups = regexp.MustCompile(`(\w+)\:\[.*?\]|(\w+)\:\d+?\s|(\w+)\:.+`)
	var reArr = regexp.MustCompile(`^\s*\[.*?\]\s*$`)
	var reNumber = regexp.MustCompile(`^\s*(?:\d*\.)?\d+\s*$`)
	var elementsGroup = reGroups.FindAllString(inputStr, -1)
	var jsonResult []string
	for _, element := range elementsGroup {
		var subEl = strings.SplitN(element, ":", 2)
		if len(subEl) < 2 {
			break
		}
		var elName = lowerFirst(strings.Trim(subEl[0], " "))
		var elValue = strings.Trim(subEl[1], " ")

		if reArr.MatchString(elValue) {
			jsonResult = append(jsonResult, fmt.Sprintf("\"%s\": [%s]", elName, processArr(elValue)))
		} else if reNumber.MatchString(elValue) {
			jsonResult = append(jsonResult, fmt.Sprintf("\"%s\": %s", elName, elValue))
		} else {
			var subNode = processLog(elValue)
			jsonResult = append(jsonResult, fmt.Sprintf("\"%s\": %s", elName, subNode))
		}
	}
	return fmt.Sprintf("{\n%s\n}", strings.Join(jsonResult, ",\n"))
}

func main() {
	fromFile, err := ioutil.ReadFile("test.log")
	check(err)
	var reJSON = regexp.MustCompile(`{(.*)}`)
	var firstPass = reJSON.FindStringSubmatch(string(fromFile))

	if len(firstPass) < 2 {
		fmt.Print("Json structure not found!")
		return
	}

	fmt.Print(processLog(firstPass[1]))
}
